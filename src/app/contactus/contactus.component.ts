import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators} from "@angular/forms";
@Component({
  selector: 'app-contactus',
  templateUrl: './contactus.component.html',
  styleUrls: ['./contactus.component.css']
})
export class ContactusComponent implements OnInit {

  contactForm: FormGroup;
  submitted: boolean = false;
  constructor() { }

  ngOnInit() {
    this.contactForm = new FormGroup({
      username : new FormControl(null, [Validators.required, Validators.minLength(5), this.blankSpaces]),
      useremail : new FormControl(null, [Validators.required, Validators.email]),
      subjectenquiry : new FormControl(null, [Validators.required, Validators.minLength(5), this.blankSpaces]),
      usermessage : new FormControl(null, [Validators.required])
    });
  }

  onSubmit(){
    console.log("Submitting form");
    console.log(this.contactForm);
    this.submitted = true
  }

  onReset(){
    this.submitted = false;
    this.contactForm.reset({
      username:"",
      useremail: "",
      subjectenquiry:"",
      usermessage:""
    });
  }

  blankSpaces(control: FormControl): {[s:string]: boolean}{

    if(control.value != null && control.value.trim().length === 0){
    return{'blankSpaces': true};
  }
    return null;
  } 
}
