import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { HorrorComponent } from './horror/horror.component';
import { ScifiComponent } from './scifi/scifi.component';
import { FamilyComponent } from './family/family.component';
import { ContactusComponent } from './contactus/contactus.component';
import { PagenotfoundComponent } from './pagenotfound/pagenotfound.component'; 

const appRoutes: Routes = [
  {path: '', redirectTo: '/home', pathMatch: 'full'},
  {path: 'home', component: HomeComponent},
  {path: 'horror', component: HorrorComponent},
  {path: 'family', component: FamilyComponent},
  {path: 'scifi', component: ScifiComponent},
  {path: 'contactus', component: ContactusComponent},
  {path: 'pagenotfound', component: PagenotfoundComponent}
]

@NgModule({
  imports: [
   RouterModule.forRoot(appRoutes)
  ],
  exports:[
    RouterModule],
})
export class AppRoutingModule { }
